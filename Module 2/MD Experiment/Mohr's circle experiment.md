# Experiment 5: <u> Mohr's Circle </u>
NIKHIL KHODAKE :
PVGCOET
---------------------
> ### Aim :

#####  To plot the Mohr's circle using direct stresses(σx,σy) and angle(θ) and to determine the values of normal stress(σn),tangential stress(σt), resultant(σR) and the angle of obliquity(ϕ).

---

>  ### Theory :  

![th01](images/th01.jpg)

- In Mohr's Circle Method, direct stress σ is represented on x-axis and shear stress τ on y-axis.

- Tensile direct stress is taken on positive x-axis and compressive direct stress on negative x-axis.

- Shear stress produced clockwise moment in the element is considered positive and anticlockwise moment is negative.

- Centre of circle is found by, C = (σx- σy)/2 , where σx and σy are direct stresses. Radius of circle, R = √[(σx- σy/2)2 + τ2]

- In Mohr's circle, the angle between the planes will be represented by double the angle.

![th02](images/th02.jpg)


> ### Procedure :

1. Click on Question 1 or 2 to display the respective questions.
2. Go through the questions carefully.
3. Enter the values of σx and σy in the input boxes respectively.Also enter value of angle.Take care that values entered of stresses are in MPa and angle in degrees.
4. Click on the arrows to toggle their directions i.e for making the normal stress tensile or compressive in nature.
5. Press submit.
6. A graph consisting of Mohr's circle will appear.
7. For determining the coordinates of various points on the graph hover the cursor on the graph.
8. Press show calculations button.
9. Entered values of σx,σy and angle will be displayed.Also formulas of Normal,Tangential,Resultant stresses and formula of angle of obliquity will be displayed.
10. Calculated values of the same will appear.

> #### RESULT:

![pro1](images/pro1.jpg)
![pro2](images/pro2.jpg)
![pro3](images/pro3.jpg)

----